const defaultTheme = require('tailwindcss/defaultTheme')

/** @type {import("@types/tailwindcss/tailwind-config").TailwindConfig } */
module.exports = {
  content: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    extend: {
      fontFamily: {
        sans: ['"Carnas"', ...defaultTheme.fontFamily.sans],
      },
      width: {
        '128': '35rem',
      },
      screens: {
        'heading-lg': '1530px',
      },
    },
    colors: {
      'primary': '#5AABA1',
      'black': '#111111',
      'gray': '#cccccc',
    }
  },
  plugins: [
    require('@tailwindcss/forms'),
    require('@tailwindcss/typography'),
    require('@tailwindcss/line-clamp'),
    require('@tailwindcss/aspect-ratio'),
  ],
}
