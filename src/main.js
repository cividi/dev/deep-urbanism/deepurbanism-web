import { createApp } from 'vue'
import './tailwind.css'
import App from './App.vue'
import router from './routes/routes.js'
import * as Fathom from 'fathom-client';
import urql from '@urql/vue';

const app = createApp(App)

Fathom.load(import.meta.env.VITE_FATHOM_ID);

app.use(urql, {
  url: import.meta.env.VITE_WAGTAIL_URL || 'http://localhost:8000/api/graphql/',
})

app.use(router)
app.mount('#app')
