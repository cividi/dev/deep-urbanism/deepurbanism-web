export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** GraphQL type for an integer that must be equal or greater than zero. */
  PositiveInt: any;
  /**
   * The `DateTime` scalar type represents a DateTime
   * value as specified by
   * [iso8601](https://en.wikipedia.org/wiki/ISO_8601).
   */
  DateTime: any;
  /**
   * Leverages the internal Python implmeentation of UUID (uuid.UUID) to provide native UUID objects
   * in fields, resolvers and input.
   */
  UUID: any;
  /**
   * Allows use of a JSON String for input / output from the GraphQL schema.
   *
   * Use of this type is *not recommended* as you lose the benefits of having a defined, static
   * schema (one of the key benefits of GraphQL).
   */
  JSONString: any;
};

export type Query = {
  __typename?: 'Query';
  reference?: Maybe<Reference>;
  references?: Maybe<Array<Maybe<Reference>>>;
  setting?: Maybe<SettingsObjectType>;
  settings: Array<SettingsObjectType>;
  redirects: Array<RedirectType>;
  collections: Array<Maybe<CollectionObjectType>>;
  tag?: Maybe<TagObjectType>;
  tags: Array<TagObjectType>;
  search: Array<Search>;
  snippets: Array<SnippetObjectType>;
  document?: Maybe<DocumentObjectType>;
  documents: Array<DocumentObjectType>;
  documentType: Scalars['String'];
  image?: Maybe<ImageObjectType>;
  images: Array<ImageObjectType>;
  imageType: Scalars['String'];
  site?: Maybe<SiteObjectType>;
  sites: Array<SiteObjectType>;
  pages: Array<PageInterface>;
  page?: Maybe<PageInterface>;
};


export type QueryReferenceArgs = {
  slug?: InputMaybe<Scalars['String']>;
};


export type QueryReferencesArgs = {
  limit?: InputMaybe<Scalars['PositiveInt']>;
  offset?: InputMaybe<Scalars['PositiveInt']>;
  order?: InputMaybe<Scalars['String']>;
  searchQuery?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};


export type QuerySettingArgs = {
  name?: InputMaybe<Scalars['String']>;
};


export type QueryCollectionsArgs = {
  limit?: InputMaybe<Scalars['PositiveInt']>;
  offset?: InputMaybe<Scalars['PositiveInt']>;
  order?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};


export type QueryTagArgs = {
  id?: InputMaybe<Scalars['ID']>;
};


export type QueryTagsArgs = {
  limit?: InputMaybe<Scalars['PositiveInt']>;
  offset?: InputMaybe<Scalars['PositiveInt']>;
  order?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};


export type QuerySearchArgs = {
  query?: InputMaybe<Scalars['String']>;
};


export type QueryDocumentArgs = {
  id?: InputMaybe<Scalars['ID']>;
};


export type QueryDocumentsArgs = {
  collection?: InputMaybe<Scalars['ID']>;
  limit?: InputMaybe<Scalars['PositiveInt']>;
  offset?: InputMaybe<Scalars['PositiveInt']>;
  order?: InputMaybe<Scalars['String']>;
  searchQuery?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};


export type QueryImageArgs = {
  id?: InputMaybe<Scalars['ID']>;
};


export type QueryImagesArgs = {
  collection?: InputMaybe<Scalars['ID']>;
  limit?: InputMaybe<Scalars['PositiveInt']>;
  offset?: InputMaybe<Scalars['PositiveInt']>;
  order?: InputMaybe<Scalars['String']>;
  searchQuery?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};


export type QuerySiteArgs = {
  hostname?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};


export type QuerySitesArgs = {
  limit?: InputMaybe<Scalars['PositiveInt']>;
  offset?: InputMaybe<Scalars['PositiveInt']>;
  order?: InputMaybe<Scalars['String']>;
  searchQuery?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};


export type QueryPagesArgs = {
  contentType?: InputMaybe<Scalars['String']>;
  inSite?: InputMaybe<Scalars['Boolean']>;
  limit?: InputMaybe<Scalars['PositiveInt']>;
  offset?: InputMaybe<Scalars['PositiveInt']>;
  order?: InputMaybe<Scalars['String']>;
  searchQuery?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};


export type QueryPageArgs = {
  id?: InputMaybe<Scalars['Int']>;
  slug?: InputMaybe<Scalars['String']>;
  urlPath?: InputMaybe<Scalars['String']>;
  token?: InputMaybe<Scalars['String']>;
  contentType?: InputMaybe<Scalars['String']>;
  inSite?: InputMaybe<Scalars['Boolean']>;
};

export type Reference = {
  __typename?: 'Reference';
  id?: Maybe<Scalars['ID']>;
  slug?: Maybe<Scalars['String']>;
  bibtex?: Maybe<Scalars['String']>;
  bibtype?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['String']>;
  bibjson?: Maybe<Scalars['String']>;
  bibtexString?: Maybe<Scalars['String']>;
};

export type SettingsObjectType = PageSettings;

export type PageSettings = {
  __typename?: 'PageSettings';
  id?: Maybe<Scalars['ID']>;
  impressumFooter?: Maybe<Scalars['String']>;
  facebook?: Maybe<Scalars['String']>;
  twitter?: Maybe<Scalars['String']>;
  instagram?: Maybe<Scalars['String']>;
  youtube?: Maybe<Scalars['String']>;
};

export type RedirectType = {
  __typename?: 'RedirectType';
  oldPath: Scalars['String'];
  oldUrl: Scalars['String'];
  newUrl: Scalars['String'];
  page?: Maybe<PageInterface>;
  isPermanent: Scalars['Boolean'];
};

export type PageInterface = {
  id?: Maybe<Scalars['ID']>;
  title: Scalars['String'];
  slug: Scalars['String'];
  contentType: Scalars['String'];
  pageType?: Maybe<Scalars['String']>;
  live: Scalars['Boolean'];
  url?: Maybe<Scalars['String']>;
  urlPath: Scalars['String'];
  depth?: Maybe<Scalars['Int']>;
  seoTitle: Scalars['String'];
  searchDescription?: Maybe<Scalars['String']>;
  showInMenus: Scalars['Boolean'];
  locked?: Maybe<Scalars['Boolean']>;
  firstPublishedAt?: Maybe<Scalars['DateTime']>;
  lastPublishedAt?: Maybe<Scalars['DateTime']>;
  parent?: Maybe<PageInterface>;
  children: Array<PageInterface>;
  siblings: Array<PageInterface>;
  nextSiblings: Array<PageInterface>;
  previousSiblings: Array<PageInterface>;
  descendants: Array<PageInterface>;
  ancestors: Array<PageInterface>;
};


export type PageInterfaceChildrenArgs = {
  limit?: InputMaybe<Scalars['PositiveInt']>;
  offset?: InputMaybe<Scalars['PositiveInt']>;
  order?: InputMaybe<Scalars['String']>;
  searchQuery?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};


export type PageInterfaceSiblingsArgs = {
  limit?: InputMaybe<Scalars['PositiveInt']>;
  offset?: InputMaybe<Scalars['PositiveInt']>;
  order?: InputMaybe<Scalars['String']>;
  searchQuery?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};


export type PageInterfaceNextSiblingsArgs = {
  limit?: InputMaybe<Scalars['PositiveInt']>;
  offset?: InputMaybe<Scalars['PositiveInt']>;
  order?: InputMaybe<Scalars['String']>;
  searchQuery?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};


export type PageInterfacePreviousSiblingsArgs = {
  limit?: InputMaybe<Scalars['PositiveInt']>;
  offset?: InputMaybe<Scalars['PositiveInt']>;
  order?: InputMaybe<Scalars['String']>;
  searchQuery?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};


export type PageInterfaceDescendantsArgs = {
  limit?: InputMaybe<Scalars['PositiveInt']>;
  offset?: InputMaybe<Scalars['PositiveInt']>;
  order?: InputMaybe<Scalars['String']>;
  searchQuery?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};


export type PageInterfaceAncestorsArgs = {
  limit?: InputMaybe<Scalars['PositiveInt']>;
  offset?: InputMaybe<Scalars['PositiveInt']>;
  order?: InputMaybe<Scalars['String']>;
  searchQuery?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};

/** Collection type */
export type CollectionObjectType = {
  __typename?: 'CollectionObjectType';
  id: Scalars['ID'];
  path: Scalars['String'];
  depth: Scalars['Int'];
  numchild: Scalars['Int'];
  name: Scalars['String'];
  descendants: Array<Maybe<CollectionObjectType>>;
  ancestors: Array<Maybe<CollectionObjectType>>;
};

export type TagObjectType = {
  __typename?: 'TagObjectType';
  id: Scalars['ID'];
  name: Scalars['String'];
};

export type Search = Publication | Chapter | Post | Page | Reference | PageSettings;

export type Publication = PageInterface & {
  __typename?: 'Publication';
  id?: Maybe<Scalars['ID']>;
  path: Scalars['String'];
  depth?: Maybe<Scalars['Int']>;
  numchild: Scalars['Int'];
  translationKey: Scalars['UUID'];
  title: Scalars['String'];
  draftTitle: Scalars['String'];
  slug: Scalars['String'];
  contentType: Scalars['String'];
  live: Scalars['Boolean'];
  hasUnpublishedChanges: Scalars['Boolean'];
  urlPath: Scalars['String'];
  seoTitle: Scalars['String'];
  showInMenus: Scalars['Boolean'];
  searchDescription?: Maybe<Scalars['String']>;
  goLiveAt?: Maybe<Scalars['DateTime']>;
  expireAt?: Maybe<Scalars['DateTime']>;
  expired: Scalars['Boolean'];
  locked?: Maybe<Scalars['Boolean']>;
  lockedAt?: Maybe<Scalars['DateTime']>;
  firstPublishedAt?: Maybe<Scalars['DateTime']>;
  lastPublishedAt?: Maybe<Scalars['DateTime']>;
  latestRevisionCreatedAt?: Maybe<Scalars['DateTime']>;
  aliasOf?: Maybe<Page>;
  subtitle?: Maybe<Scalars['String']>;
  abstract?: Maybe<Scalars['String']>;
  staged?: Maybe<Scalars['Boolean']>;
  pageType?: Maybe<Scalars['String']>;
  url?: Maybe<Scalars['String']>;
  parent?: Maybe<PageInterface>;
  children: Array<PageInterface>;
  siblings: Array<PageInterface>;
  nextSiblings: Array<PageInterface>;
  previousSiblings: Array<PageInterface>;
  descendants: Array<PageInterface>;
  ancestors: Array<PageInterface>;
};


export type PublicationChildrenArgs = {
  limit?: InputMaybe<Scalars['PositiveInt']>;
  offset?: InputMaybe<Scalars['PositiveInt']>;
  order?: InputMaybe<Scalars['String']>;
  searchQuery?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};


export type PublicationSiblingsArgs = {
  limit?: InputMaybe<Scalars['PositiveInt']>;
  offset?: InputMaybe<Scalars['PositiveInt']>;
  order?: InputMaybe<Scalars['String']>;
  searchQuery?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};


export type PublicationNextSiblingsArgs = {
  limit?: InputMaybe<Scalars['PositiveInt']>;
  offset?: InputMaybe<Scalars['PositiveInt']>;
  order?: InputMaybe<Scalars['String']>;
  searchQuery?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};


export type PublicationPreviousSiblingsArgs = {
  limit?: InputMaybe<Scalars['PositiveInt']>;
  offset?: InputMaybe<Scalars['PositiveInt']>;
  order?: InputMaybe<Scalars['String']>;
  searchQuery?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};


export type PublicationDescendantsArgs = {
  limit?: InputMaybe<Scalars['PositiveInt']>;
  offset?: InputMaybe<Scalars['PositiveInt']>;
  order?: InputMaybe<Scalars['String']>;
  searchQuery?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};


export type PublicationAncestorsArgs = {
  limit?: InputMaybe<Scalars['PositiveInt']>;
  offset?: InputMaybe<Scalars['PositiveInt']>;
  order?: InputMaybe<Scalars['String']>;
  searchQuery?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};

/**
 * Base Page type used if one isn't generated for the current model.
 * All other node types extend this.
 */
export type Page = PageInterface & {
  __typename?: 'Page';
  id?: Maybe<Scalars['ID']>;
  path: Scalars['String'];
  depth?: Maybe<Scalars['Int']>;
  numchild: Scalars['Int'];
  translationKey: Scalars['UUID'];
  title: Scalars['String'];
  draftTitle: Scalars['String'];
  slug: Scalars['String'];
  contentType: Scalars['String'];
  live: Scalars['Boolean'];
  hasUnpublishedChanges: Scalars['Boolean'];
  urlPath: Scalars['String'];
  seoTitle: Scalars['String'];
  showInMenus: Scalars['Boolean'];
  searchDescription?: Maybe<Scalars['String']>;
  goLiveAt?: Maybe<Scalars['DateTime']>;
  expireAt?: Maybe<Scalars['DateTime']>;
  expired: Scalars['Boolean'];
  locked?: Maybe<Scalars['Boolean']>;
  lockedAt?: Maybe<Scalars['DateTime']>;
  firstPublishedAt?: Maybe<Scalars['DateTime']>;
  lastPublishedAt?: Maybe<Scalars['DateTime']>;
  latestRevisionCreatedAt?: Maybe<Scalars['DateTime']>;
  aliasOf?: Maybe<Page>;
  sitesRootedHere: Array<SiteObjectType>;
  aliases: Array<Page>;
  publication?: Maybe<Publication>;
  chapter?: Maybe<Chapter>;
  post?: Maybe<Post>;
  pageType?: Maybe<Scalars['String']>;
  url?: Maybe<Scalars['String']>;
  parent?: Maybe<PageInterface>;
  children: Array<PageInterface>;
  siblings: Array<PageInterface>;
  nextSiblings: Array<PageInterface>;
  previousSiblings: Array<PageInterface>;
  descendants: Array<PageInterface>;
  ancestors: Array<PageInterface>;
};


/**
 * Base Page type used if one isn't generated for the current model.
 * All other node types extend this.
 */
export type PageChildrenArgs = {
  limit?: InputMaybe<Scalars['PositiveInt']>;
  offset?: InputMaybe<Scalars['PositiveInt']>;
  order?: InputMaybe<Scalars['String']>;
  searchQuery?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};


/**
 * Base Page type used if one isn't generated for the current model.
 * All other node types extend this.
 */
export type PageSiblingsArgs = {
  limit?: InputMaybe<Scalars['PositiveInt']>;
  offset?: InputMaybe<Scalars['PositiveInt']>;
  order?: InputMaybe<Scalars['String']>;
  searchQuery?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};


/**
 * Base Page type used if one isn't generated for the current model.
 * All other node types extend this.
 */
export type PageNextSiblingsArgs = {
  limit?: InputMaybe<Scalars['PositiveInt']>;
  offset?: InputMaybe<Scalars['PositiveInt']>;
  order?: InputMaybe<Scalars['String']>;
  searchQuery?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};


/**
 * Base Page type used if one isn't generated for the current model.
 * All other node types extend this.
 */
export type PagePreviousSiblingsArgs = {
  limit?: InputMaybe<Scalars['PositiveInt']>;
  offset?: InputMaybe<Scalars['PositiveInt']>;
  order?: InputMaybe<Scalars['String']>;
  searchQuery?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};


/**
 * Base Page type used if one isn't generated for the current model.
 * All other node types extend this.
 */
export type PageDescendantsArgs = {
  limit?: InputMaybe<Scalars['PositiveInt']>;
  offset?: InputMaybe<Scalars['PositiveInt']>;
  order?: InputMaybe<Scalars['String']>;
  searchQuery?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};


/**
 * Base Page type used if one isn't generated for the current model.
 * All other node types extend this.
 */
export type PageAncestorsArgs = {
  limit?: InputMaybe<Scalars['PositiveInt']>;
  offset?: InputMaybe<Scalars['PositiveInt']>;
  order?: InputMaybe<Scalars['String']>;
  searchQuery?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};

export type SiteObjectType = {
  __typename?: 'SiteObjectType';
  id: Scalars['ID'];
  hostname: Scalars['String'];
  /** Set this to something other than 80 if you need a specific port number to appear in URLs (e.g. development on port 8000). Does not affect request handling (so port forwarding still works). */
  port: Scalars['Int'];
  /** Human-readable name for the site. */
  siteName: Scalars['String'];
  rootPage: Page;
  /** If true, this site will handle requests for all other hostnames that do not have a site entry of their own */
  isDefaultSite: Scalars['Boolean'];
  pagesettings?: Maybe<PageSettings>;
  pages: Array<PageInterface>;
  page?: Maybe<PageInterface>;
};


export type SiteObjectTypePagesArgs = {
  contentType?: InputMaybe<Scalars['String']>;
  limit?: InputMaybe<Scalars['PositiveInt']>;
  offset?: InputMaybe<Scalars['PositiveInt']>;
  order?: InputMaybe<Scalars['String']>;
  searchQuery?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};


export type SiteObjectTypePageArgs = {
  id?: InputMaybe<Scalars['Int']>;
  slug?: InputMaybe<Scalars['String']>;
  token?: InputMaybe<Scalars['String']>;
  contentType?: InputMaybe<Scalars['String']>;
};

export type Chapter = PageInterface & {
  __typename?: 'Chapter';
  id?: Maybe<Scalars['ID']>;
  path: Scalars['String'];
  depth?: Maybe<Scalars['Int']>;
  numchild: Scalars['Int'];
  translationKey: Scalars['UUID'];
  title: Scalars['String'];
  draftTitle: Scalars['String'];
  slug: Scalars['String'];
  contentType: Scalars['String'];
  live: Scalars['Boolean'];
  hasUnpublishedChanges: Scalars['Boolean'];
  urlPath: Scalars['String'];
  seoTitle: Scalars['String'];
  showInMenus: Scalars['Boolean'];
  searchDescription?: Maybe<Scalars['String']>;
  goLiveAt?: Maybe<Scalars['DateTime']>;
  expireAt?: Maybe<Scalars['DateTime']>;
  expired: Scalars['Boolean'];
  locked?: Maybe<Scalars['Boolean']>;
  lockedAt?: Maybe<Scalars['DateTime']>;
  firstPublishedAt?: Maybe<Scalars['DateTime']>;
  lastPublishedAt?: Maybe<Scalars['DateTime']>;
  latestRevisionCreatedAt?: Maybe<Scalars['DateTime']>;
  aliasOf?: Maybe<Page>;
  abstract?: Maybe<Scalars['String']>;
  staged?: Maybe<Scalars['Boolean']>;
  pageType?: Maybe<Scalars['String']>;
  url?: Maybe<Scalars['String']>;
  parent?: Maybe<PageInterface>;
  children: Array<PageInterface>;
  siblings: Array<PageInterface>;
  nextSiblings: Array<PageInterface>;
  previousSiblings: Array<PageInterface>;
  descendants: Array<PageInterface>;
  ancestors: Array<PageInterface>;
};


export type ChapterChildrenArgs = {
  limit?: InputMaybe<Scalars['PositiveInt']>;
  offset?: InputMaybe<Scalars['PositiveInt']>;
  order?: InputMaybe<Scalars['String']>;
  searchQuery?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};


export type ChapterSiblingsArgs = {
  limit?: InputMaybe<Scalars['PositiveInt']>;
  offset?: InputMaybe<Scalars['PositiveInt']>;
  order?: InputMaybe<Scalars['String']>;
  searchQuery?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};


export type ChapterNextSiblingsArgs = {
  limit?: InputMaybe<Scalars['PositiveInt']>;
  offset?: InputMaybe<Scalars['PositiveInt']>;
  order?: InputMaybe<Scalars['String']>;
  searchQuery?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};


export type ChapterPreviousSiblingsArgs = {
  limit?: InputMaybe<Scalars['PositiveInt']>;
  offset?: InputMaybe<Scalars['PositiveInt']>;
  order?: InputMaybe<Scalars['String']>;
  searchQuery?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};


export type ChapterDescendantsArgs = {
  limit?: InputMaybe<Scalars['PositiveInt']>;
  offset?: InputMaybe<Scalars['PositiveInt']>;
  order?: InputMaybe<Scalars['String']>;
  searchQuery?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};


export type ChapterAncestorsArgs = {
  limit?: InputMaybe<Scalars['PositiveInt']>;
  offset?: InputMaybe<Scalars['PositiveInt']>;
  order?: InputMaybe<Scalars['String']>;
  searchQuery?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};

export type Post = PageInterface & {
  __typename?: 'Post';
  id?: Maybe<Scalars['ID']>;
  path: Scalars['String'];
  depth?: Maybe<Scalars['Int']>;
  numchild: Scalars['Int'];
  translationKey: Scalars['UUID'];
  title: Scalars['String'];
  draftTitle: Scalars['String'];
  slug: Scalars['String'];
  contentType: Scalars['String'];
  live: Scalars['Boolean'];
  hasUnpublishedChanges: Scalars['Boolean'];
  urlPath: Scalars['String'];
  seoTitle: Scalars['String'];
  showInMenus: Scalars['Boolean'];
  searchDescription?: Maybe<Scalars['String']>;
  goLiveAt?: Maybe<Scalars['DateTime']>;
  expireAt?: Maybe<Scalars['DateTime']>;
  expired: Scalars['Boolean'];
  locked?: Maybe<Scalars['Boolean']>;
  lockedAt?: Maybe<Scalars['DateTime']>;
  firstPublishedAt?: Maybe<Scalars['DateTime']>;
  lastPublishedAt?: Maybe<Scalars['DateTime']>;
  latestRevisionCreatedAt?: Maybe<Scalars['DateTime']>;
  aliasOf?: Maybe<Page>;
  abstract?: Maybe<Scalars['String']>;
  body?: Maybe<Array<Maybe<StreamFieldInterface>>>;
  staged?: Maybe<Scalars['Boolean']>;
  pageType?: Maybe<Scalars['String']>;
  url?: Maybe<Scalars['String']>;
  parent?: Maybe<PageInterface>;
  children: Array<PageInterface>;
  siblings: Array<PageInterface>;
  nextSiblings: Array<PageInterface>;
  previousSiblings: Array<PageInterface>;
  descendants: Array<PageInterface>;
  ancestors: Array<PageInterface>;
};


export type PostChildrenArgs = {
  limit?: InputMaybe<Scalars['PositiveInt']>;
  offset?: InputMaybe<Scalars['PositiveInt']>;
  order?: InputMaybe<Scalars['String']>;
  searchQuery?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};


export type PostSiblingsArgs = {
  limit?: InputMaybe<Scalars['PositiveInt']>;
  offset?: InputMaybe<Scalars['PositiveInt']>;
  order?: InputMaybe<Scalars['String']>;
  searchQuery?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};


export type PostNextSiblingsArgs = {
  limit?: InputMaybe<Scalars['PositiveInt']>;
  offset?: InputMaybe<Scalars['PositiveInt']>;
  order?: InputMaybe<Scalars['String']>;
  searchQuery?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};


export type PostPreviousSiblingsArgs = {
  limit?: InputMaybe<Scalars['PositiveInt']>;
  offset?: InputMaybe<Scalars['PositiveInt']>;
  order?: InputMaybe<Scalars['String']>;
  searchQuery?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};


export type PostDescendantsArgs = {
  limit?: InputMaybe<Scalars['PositiveInt']>;
  offset?: InputMaybe<Scalars['PositiveInt']>;
  order?: InputMaybe<Scalars['String']>;
  searchQuery?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};


export type PostAncestorsArgs = {
  limit?: InputMaybe<Scalars['PositiveInt']>;
  offset?: InputMaybe<Scalars['PositiveInt']>;
  order?: InputMaybe<Scalars['String']>;
  searchQuery?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};

export type StreamFieldInterface = {
  id?: Maybe<Scalars['String']>;
  blockType: Scalars['String'];
  field: Scalars['String'];
  rawValue: Scalars['String'];
};

export type SnippetObjectType = Reference;

/**
 * Base document type used if one isn't generated for the current model.
 * All other node types extend this.
 */
export type DocumentObjectType = {
  __typename?: 'DocumentObjectType';
  id: Scalars['ID'];
  collection: CollectionObjectType;
  title: Scalars['String'];
  file: Scalars['String'];
  createdAt: Scalars['DateTime'];
  fileSize?: Maybe<Scalars['Int']>;
  fileHash: Scalars['String'];
  url: Scalars['String'];
  tags: Array<TagObjectType>;
};

export type ImageObjectType = {
  __typename?: 'ImageObjectType';
  id: Scalars['ID'];
  collection: CollectionObjectType;
  title: Scalars['String'];
  file: Scalars['String'];
  width: Scalars['Int'];
  height: Scalars['Int'];
  createdAt: Scalars['DateTime'];
  focalPointX?: Maybe<Scalars['Int']>;
  focalPointY?: Maybe<Scalars['Int']>;
  focalPointWidth?: Maybe<Scalars['Int']>;
  focalPointHeight?: Maybe<Scalars['Int']>;
  fileSize?: Maybe<Scalars['Int']>;
  fileHash: Scalars['String'];
  renditions: Array<ImageRenditionObjectType>;
  /** @deprecated Use the `url` attribute */
  src: Scalars['String'];
  url: Scalars['String'];
  aspectRatio: Scalars['Float'];
  sizes: Scalars['String'];
  tags: Array<TagObjectType>;
  rendition?: Maybe<ImageRenditionObjectType>;
  srcSet?: Maybe<Scalars['String']>;
};


export type ImageObjectTypeRenditionArgs = {
  max?: InputMaybe<Scalars['String']>;
  min?: InputMaybe<Scalars['String']>;
  width?: InputMaybe<Scalars['Int']>;
  height?: InputMaybe<Scalars['Int']>;
  fill?: InputMaybe<Scalars['String']>;
  format?: InputMaybe<Scalars['String']>;
  bgcolor?: InputMaybe<Scalars['String']>;
  jpegquality?: InputMaybe<Scalars['Int']>;
  webpquality?: InputMaybe<Scalars['Int']>;
};


export type ImageObjectTypeSrcSetArgs = {
  sizes?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
};

export type ImageRenditionObjectType = {
  __typename?: 'ImageRenditionObjectType';
  id: Scalars['ID'];
  filterSpec: Scalars['String'];
  file: Scalars['String'];
  width: Scalars['Int'];
  height: Scalars['Int'];
  focalPointKey: Scalars['String'];
  image: ImageObjectType;
  title: Scalars['String'];
  createdAt: Scalars['DateTime'];
  focalPointX?: Maybe<Scalars['Int']>;
  focalPointY?: Maybe<Scalars['Int']>;
  focalPointWidth?: Maybe<Scalars['Int']>;
  focalPointHeight?: Maybe<Scalars['Int']>;
  fileSize?: Maybe<Scalars['Int']>;
  fileHash: Scalars['String'];
  /** @deprecated Use the `url` attribute */
  src: Scalars['String'];
  url: Scalars['String'];
  aspectRatio: Scalars['Float'];
  sizes: Scalars['String'];
  collection: CollectionObjectType;
  tags: Array<TagObjectType>;
};

export type Subscription = {
  __typename?: 'Subscription';
  page?: Maybe<PageInterface>;
};


export type SubscriptionPageArgs = {
  id?: InputMaybe<Scalars['Int']>;
  slug?: InputMaybe<Scalars['String']>;
  urlPath?: InputMaybe<Scalars['String']>;
  token?: InputMaybe<Scalars['String']>;
  contentType?: InputMaybe<Scalars['String']>;
  inSite?: InputMaybe<Scalars['Boolean']>;
};

export type StreamFieldBlock = StreamFieldInterface & {
  __typename?: 'StreamFieldBlock';
  id?: Maybe<Scalars['String']>;
  blockType: Scalars['String'];
  field: Scalars['String'];
  rawValue: Scalars['String'];
  value: Scalars['String'];
};

export type CharBlock = StreamFieldInterface & {
  __typename?: 'CharBlock';
  id?: Maybe<Scalars['String']>;
  blockType: Scalars['String'];
  field: Scalars['String'];
  rawValue: Scalars['String'];
  value: Scalars['String'];
};

export type TextBlock = StreamFieldInterface & {
  __typename?: 'TextBlock';
  id?: Maybe<Scalars['String']>;
  blockType: Scalars['String'];
  field: Scalars['String'];
  rawValue: Scalars['String'];
  value: Scalars['String'];
};

export type EmailBlock = StreamFieldInterface & {
  __typename?: 'EmailBlock';
  id?: Maybe<Scalars['String']>;
  blockType: Scalars['String'];
  field: Scalars['String'];
  rawValue: Scalars['String'];
  value: Scalars['String'];
};

export type IntegerBlock = StreamFieldInterface & {
  __typename?: 'IntegerBlock';
  id?: Maybe<Scalars['String']>;
  blockType: Scalars['String'];
  field: Scalars['String'];
  rawValue: Scalars['String'];
  value: Scalars['Int'];
};

export type FloatBlock = StreamFieldInterface & {
  __typename?: 'FloatBlock';
  id?: Maybe<Scalars['String']>;
  blockType: Scalars['String'];
  field: Scalars['String'];
  rawValue: Scalars['String'];
  value: Scalars['Float'];
};

export type DecimalBlock = StreamFieldInterface & {
  __typename?: 'DecimalBlock';
  id?: Maybe<Scalars['String']>;
  blockType: Scalars['String'];
  field: Scalars['String'];
  rawValue: Scalars['String'];
  value: Scalars['Float'];
};

export type RegexBlock = StreamFieldInterface & {
  __typename?: 'RegexBlock';
  id?: Maybe<Scalars['String']>;
  blockType: Scalars['String'];
  field: Scalars['String'];
  rawValue: Scalars['String'];
  value: Scalars['String'];
};

export type UrlBlock = StreamFieldInterface & {
  __typename?: 'URLBlock';
  id?: Maybe<Scalars['String']>;
  blockType: Scalars['String'];
  field: Scalars['String'];
  rawValue: Scalars['String'];
  value: Scalars['String'];
};

export type BooleanBlock = StreamFieldInterface & {
  __typename?: 'BooleanBlock';
  id?: Maybe<Scalars['String']>;
  blockType: Scalars['String'];
  field: Scalars['String'];
  rawValue: Scalars['String'];
  value: Scalars['Boolean'];
};

export type DateBlock = StreamFieldInterface & {
  __typename?: 'DateBlock';
  id?: Maybe<Scalars['String']>;
  blockType: Scalars['String'];
  field: Scalars['String'];
  rawValue: Scalars['String'];
  value: Scalars['String'];
};


export type DateBlockValueArgs = {
  format?: InputMaybe<Scalars['String']>;
};

export type TimeBlock = StreamFieldInterface & {
  __typename?: 'TimeBlock';
  id?: Maybe<Scalars['String']>;
  blockType: Scalars['String'];
  field: Scalars['String'];
  rawValue: Scalars['String'];
  value: Scalars['String'];
};


export type TimeBlockValueArgs = {
  format?: InputMaybe<Scalars['String']>;
};

export type DateTimeBlock = StreamFieldInterface & {
  __typename?: 'DateTimeBlock';
  id?: Maybe<Scalars['String']>;
  blockType: Scalars['String'];
  field: Scalars['String'];
  rawValue: Scalars['String'];
  value: Scalars['String'];
};


export type DateTimeBlockValueArgs = {
  format?: InputMaybe<Scalars['String']>;
};

export type RichTextBlock = StreamFieldInterface & {
  __typename?: 'RichTextBlock';
  id?: Maybe<Scalars['String']>;
  blockType: Scalars['String'];
  field: Scalars['String'];
  rawValue: Scalars['String'];
  value: Scalars['String'];
};

export type RawHtmlBlock = StreamFieldInterface & {
  __typename?: 'RawHTMLBlock';
  id?: Maybe<Scalars['String']>;
  blockType: Scalars['String'];
  field: Scalars['String'];
  rawValue: Scalars['String'];
  value: Scalars['String'];
};

export type BlockQuoteBlock = StreamFieldInterface & {
  __typename?: 'BlockQuoteBlock';
  id?: Maybe<Scalars['String']>;
  blockType: Scalars['String'];
  field: Scalars['String'];
  rawValue: Scalars['String'];
  value: Scalars['String'];
};

export type ChoiceBlock = StreamFieldInterface & {
  __typename?: 'ChoiceBlock';
  id?: Maybe<Scalars['String']>;
  blockType: Scalars['String'];
  field: Scalars['String'];
  rawValue: Scalars['String'];
  value: Scalars['String'];
  choices: Array<ChoiceOption>;
};

export type ChoiceOption = {
  __typename?: 'ChoiceOption';
  key: Scalars['String'];
  value: Scalars['String'];
};

export type StreamBlock = StreamFieldInterface & {
  __typename?: 'StreamBlock';
  id?: Maybe<Scalars['String']>;
  blockType: Scalars['String'];
  field: Scalars['String'];
  rawValue: Scalars['String'];
  blocks: Array<StreamFieldInterface>;
};

export type StructBlock = StreamFieldInterface & {
  __typename?: 'StructBlock';
  id?: Maybe<Scalars['String']>;
  blockType: Scalars['String'];
  field: Scalars['String'];
  rawValue: Scalars['String'];
  blocks: Array<StreamFieldInterface>;
};

export type StaticBlock = StreamFieldInterface & {
  __typename?: 'StaticBlock';
  id?: Maybe<Scalars['String']>;
  blockType: Scalars['String'];
  field: Scalars['String'];
  rawValue: Scalars['String'];
  value: Scalars['String'];
};

export type ListBlock = StreamFieldInterface & {
  __typename?: 'ListBlock';
  id?: Maybe<Scalars['String']>;
  blockType: Scalars['String'];
  field: Scalars['String'];
  rawValue: Scalars['String'];
  items: Array<StreamFieldInterface>;
};

export type EmbedBlock = StreamFieldInterface & {
  __typename?: 'EmbedBlock';
  id?: Maybe<Scalars['String']>;
  blockType: Scalars['String'];
  field: Scalars['String'];
  rawValue: Scalars['String'];
  value: Scalars['String'];
  url: Scalars['String'];
  embed?: Maybe<Scalars['String']>;
  rawEmbed?: Maybe<Scalars['JSONString']>;
};

export type PageChooserBlock = StreamFieldInterface & {
  __typename?: 'PageChooserBlock';
  id?: Maybe<Scalars['String']>;
  blockType: Scalars['String'];
  field: Scalars['String'];
  rawValue: Scalars['String'];
  page: PageInterface;
};

export type DocumentChooserBlock = StreamFieldInterface & {
  __typename?: 'DocumentChooserBlock';
  id?: Maybe<Scalars['String']>;
  blockType: Scalars['String'];
  field: Scalars['String'];
  rawValue: Scalars['String'];
  document: DocumentObjectType;
};

export type ImageChooserBlock = StreamFieldInterface & {
  __typename?: 'ImageChooserBlock';
  id?: Maybe<Scalars['String']>;
  blockType: Scalars['String'];
  field: Scalars['String'];
  rawValue: Scalars['String'];
  image: ImageObjectType;
};

export type SnippetChooserBlock = StreamFieldInterface & {
  __typename?: 'SnippetChooserBlock';
  id?: Maybe<Scalars['String']>;
  blockType: Scalars['String'];
  field: Scalars['String'];
  rawValue: Scalars['String'];
  snippet: Scalars['String'];
};
