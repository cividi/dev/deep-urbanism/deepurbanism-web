import { createRouter, createWebHistory} from 'vue-router'
import Publication from '../views/Publication.vue'

const routes = [
  { path: '/', component: Publication, meta: { title: 'Publication' } },
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router