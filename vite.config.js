import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import Vql from 'vite-plugin-vue-gql'
import { resolve } from 'path'

export default defineConfig({
  plugins: [
    vue(),
    Vql()
  ],
  resolve: {
    alias: {
      '@': resolve(__dirname, 'src'),
    },
  },
  server: {
    open: true,
  },
  define: {
    'process.platform': null,
    'process.version': null,
  }
  // optimizeDeps: {
  //   include: [
  //       '@urql/vue',
  //       'graphql'
  //   ]
  // },
  // rollupInputOptions: {
  //   external: ['react']
  // }
})
